class User < ApplicationRecord
  validates :reserved_username, presence: true
  validates :insta_username, presence: true
  validates :email, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
end
