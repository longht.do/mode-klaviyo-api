require 'uri'
require 'net/http'
require 'openssl'

class Klaviyo
  LIST_ID = 'Ybr8JN'
  PRIVATE_KEY = 'pk_3ee38c812926091b7c3cb517b6ddff7c9d'

  def self.add_member_to_list(user)
    puts user.to_json
    url = URI("https://a.klaviyo.com/api/v2/list/#{LIST_ID}/members?api_key=#{PRIVATE_KEY}")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(url)
    request['Accept'] = 'application/json'
    request['Content-type'] = 'application/json'
    request.body = {
      profiles: [
        {
          reserved_username: user.reserved_username,
          insta_username: user.insta_username,
          email: user.email,
          first_name: user.first_name,
          last_name: user.last_name,
        }
      ]
    }.to_json

    response = http.request(request)
    puts response.read_body
  end
end
