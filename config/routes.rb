Rails.application.routes.draw do
  resources :users do
    post '/sign_up', to: 'users#create', on: :collection
    put '/:id', to: 'users#update', on: :collection
    delete '/:id', to: 'users#destroy', on: :collection
  end
end
